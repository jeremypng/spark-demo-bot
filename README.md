#Spark-Demo-Bot README

* based on Flint and sparkstarterbot NPM packages
* Jeremy Sanders jsanders@teklinks.com

##Requires:
* node-flint
* express
* body-parser
* request

##Usage:

1. Create Spark Bot on developer.cisco.com. Collect the token for usage in the config.json file.

2. Install NPM and Node.js
* For example, on CentOS sudo or as root:
```
yum install -y gcc-c++ make
curl -sL https://rpm.nodesource.com/setup_6.x | sudo -E bash -
yum install nodejs
```
As non-privileged user in homedir:
```
git clone https://jeremypng@bitbucket.org/jeremypng/spark-demo-bot.git

cd spark-demo-bot

npm install node-flint
npm install express
npm install body-parser
npm install request

cp config.json.default config.json
```
* <change appropriate values in config.json>

3. Start bot
```
node.js --use-strict index.js
```

4. optionally install pm2 to manage process
```
sudo npm -g install pm2
pm2 start index.js
```

5. Modify or extend index.js as needed for your purposes.
